package com.example.jhamel.gardenapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

//This is the first screen of the first garden.
public class Harwood_1 extends AppCompatActivity {

    //creates a reference variables for the buttons
    Button btnRight, btnQues1_1_1, btnQues1_1_2, btnQues1_1_3, btn_map;

    //**These are constant fields that are used to tell the program where each button is located.
    // garden refers to which garden we are in.
    //Screen tells the current screen. Each button field is the location of a different button.
    final int garden = 1;
    final int screen = 1;
    final int button1_1_1 = 1;
    final int button1_1_2 = 2;
    final int button1_1_3 = 3;


    //This is the overrides method for onCreate.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set the layout
        setContentView(R.layout.activity_harwood_1);

        SaveData.currentGarden = garden;
        SaveData.currentScreen = screen;

        //have our reference variables point to the buttons in the layout file.
        btnQues1_1_1 = (Button) findViewById(R.id.btnQ1_1_1);
        btnQues1_1_2 = (Button) findViewById(R.id.btnQ1_1_2);
        btnQues1_1_3 = (Button) findViewById(R.id.btnQ1_1_3);
        btn_map = (Button) findViewById(R.id.btn_goToMap_1_1);
        btnRight = (Button) findViewById(R.id.btnRight1);

        //Set a listener for the right arrow button that will bring the user to the Harwood_2 activity when clicked.
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //set up an intent that will direct the user to harwood_2.
                Intent intent = new Intent(v.getContext(), Harwood_2.class);
                //starts the activity with our new intent.
                startActivity(intent);
            }
        });

        btn_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.setGoToMap(true);
               finish();
            }
        });

        //set a listener for our black button that will direct the user to our Question activity when clicked.
        //**this sets up the text for the question and the button by passing strings into fields located inside of the QuestionData class
        //One of these must be created for each new button.**

        // Firepower
        btnQues1_1_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.currentButton = button1_1_1;
                QuestionData.setCorrectAnswer(3);
                QuestionData.setQuestionText("This plant can be poisonous to grazing animals and is " +
                        "considered invasive in states like North Carolina");
                QuestionData.setButtonText(1, "Paperbush");
                QuestionData.setButtonText(2, "Winter Wine");
                QuestionData.setButtonText(3, "Firepower");
                QuestionData.setButtonText(4, "Azalea");
                Intent intent = new Intent(v.getContext(), Question.class);
                startActivity(intent);
            }
        });

        //*set listener for our second question button.**
        // Distylium myricoides
        btnQues1_1_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.currentButton = button1_1_2;
                QuestionData.setCorrectAnswer(4);
                QuestionData.setQuestionText("This shrub is know for its petalless flowers that consist" +
                        "of red stamens and blue/green leaves:");
                QuestionData.setButtonText(1, "Gardenia angusta 'Frostproof'");
                QuestionData.setButtonText(2, "Hebe pimeleoides 'Quicksilver'");
                QuestionData.setButtonText(3, "Gomphostigma virgatum");
                QuestionData.setButtonText(4, "Distylium myricoides");
                Intent intent = new Intent(v.getContext(), Question.class);
                startActivity(intent);
            }
        });

        // Sterling Silver
        btnQues1_1_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.currentButton = button1_1_3;
                QuestionData.setCorrectAnswer(1);
                QuestionData.setQuestionText("This plant is yellow, flowers in the winter and typically grows 6'-8' tall:");
                QuestionData.setButtonText(1, "Stachyurus praecox 'Sterling Silver'");
                QuestionData.setButtonText(2, "Nandina domestica 'Firepower'");
                QuestionData.setButtonText(3, "Buxus x 'Winter Gem'");
                QuestionData.setButtonText(4, "Vinca minor 'Periwinkle'");
                Intent intent = new Intent(v.getContext(), Question.class);
                startActivity(intent);
            }
        });



    }

    //This method overrides the onResume() function of the superclass. This code will automatically execute each time this
    //activity regains control. I assume it will also execute any time that the onCreate() method executes; however, this has not
    //been tested yet. I have overridden the function to check the buttonData array in order to check if any questions had been
    //answered correctly. If the question has been answered correctly the button will become invisible. A side effect is that the
    //button becomes unclickable. We will need to implement a workaround for this.
    //**We need to now add a new, individual if-else statement for every button that we have on the screen**
    @Override
    protected void onResume() {
        super.onResume();
        if(SaveData.getButtonData(garden, screen, button1_1_1)){
            btnQues1_1_1.setVisibility(View.INVISIBLE);
        }
        else{
            btnQues1_1_1.setVisibility(View.VISIBLE);
        }

        if(SaveData.getButtonData(garden, screen, button1_1_2)){
            btnQues1_1_2.setVisibility(View.INVISIBLE);
        }
        else{
            btnQues1_1_2.setVisibility(View.VISIBLE);
        }

        if(SaveData.getButtonData(garden, screen, button1_1_3)){
            btnQues1_1_3.setVisibility(View.INVISIBLE);
        }
        else{
            btnQues1_1_3.setVisibility(View.VISIBLE);
        }

        if(SaveData.getGoToMap() == true){
            finish();
        }
    }
}