package com.example.jhamel.gardenapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


//This is the activity is what the user first sees when the app is started up. This is our navigation screen that contains the
//map of the garden as well as clickable buttons for the users to click when they enter a location. By design this activity will
//ALWAYS be in memory. In other words, if it is not active, it will be on the stack in memory. This is because all other activities
//will eventually return back to this one. Because of this, avoid adding memory intensive functions to this class whenever possible and
//avoid adding large pictures to the layout file for this class.
public class MainActivity extends AppCompatActivity {

    //creates a reference variable for a button
    Button btnBlu, btnSet;


    //This function was automatically when making the class. This is code is executed each time this activity is created. This should
    //only happen once when the user starts the program up. It overrides the onCreate method of its superclass.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SaveData.setGoToMap(false);

        //set up the layout for the activity.
        setContentView(R.layout.activity_main);

        //reset all the data.
        SaveData.resetButtonData();

        //have our reference variable point to our button in the layout file.
        btnBlu = (Button) findViewById(R.id.btnBlue);
        btnSet = (Button) findViewById(R.id.btn_settings);


        //set up a listener for the blue button that will allow users to go to the first garden when clicked.
        btnBlu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), Harwood_1.class);
                startActivity(i);
            }
        });

        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), Settings.class);
                startActivity(i);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        SaveData.setGoToMap(false);
    }
}
