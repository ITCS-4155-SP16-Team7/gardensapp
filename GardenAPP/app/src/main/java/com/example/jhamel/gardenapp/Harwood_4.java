package com.example.jhamel.gardenapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Harwood_4 extends AppCompatActivity {

    Button btnRight, btnLeft, btnQues1_4_1, btnQues1_4_2, btn_map;

    final int garden = 1;
    final int screen = 4;
    final int button1_4_1 = 1;
    final int button1_4_2 = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_harwood_4);

        SaveData.currentGarden = garden;
        SaveData.currentScreen = screen;

        btnRight = (Button) findViewById(R.id.btnRight4);
        btnLeft = (Button) findViewById(R.id.btnLeft4);
        btnQues1_4_1 = (Button) findViewById(R.id.btnQ1_4_1);
        btnQues1_4_2 = (Button) findViewById(R.id.btnQ1_4_2);
        btn_map = (Button) findViewById(R.id.btn_goToMap_1_4);


        btn_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.setGoToMap(true);
                finish();
            }
        });


        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), Harwood_5.class);
                startActivity(intent);
            }
        });

        // Royal Princess
        btnQues1_4_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.currentButton = button1_4_1;
                QuestionData.setCorrectAnswer(3);
                QuestionData.setQuestionText("This beautiful shrub originates in central asia?");
                QuestionData.setButtonText(1, "Phlox ‘David’");
                QuestionData.setButtonText(2, "Penstemon digitalis 'Husker Red'");
                QuestionData.setButtonText(3, "Nandina domestica 'Royal Princess'");
                QuestionData.setButtonText(4, "Heuchera micrantha 'Palace Purple'");
                Intent intent = new Intent(v.getContext(), Question.class);
                startActivity(intent);
            }
        });

        //*set listener for our second question button.**
        // Magnolia
        btnQues1_4_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.currentButton = button1_4_2;
                QuestionData.setCorrectAnswer(2);
                QuestionData.setQuestionText("This tree goes by the common name manglietia:");
                QuestionData.setButtonText(1, "Acer palmatum");
                QuestionData.setButtonText(2, "Magnolia yuyuanesis");
                QuestionData.setButtonText(3, "Leyland Cypress");
                QuestionData.setButtonText(4, "Rudbeckia hirta");
                Intent intent = new Intent(v.getContext(), Question.class);
                startActivity(intent);
            }
        });

        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(SaveData.getButtonData(garden, screen, button1_4_1)){
            btnQues1_4_1.setVisibility(View.INVISIBLE);
        }
        else{
            btnQues1_4_1.setVisibility(View.VISIBLE);
        }

        if(SaveData.getButtonData(garden, screen, button1_4_2)){
            btnQues1_4_2.setVisibility(View.INVISIBLE);
        }
        else{
            btnQues1_4_2.setVisibility(View.VISIBLE);
        }

        if(SaveData.getGoToMap() == true){
            finish();
        }
    }
}
