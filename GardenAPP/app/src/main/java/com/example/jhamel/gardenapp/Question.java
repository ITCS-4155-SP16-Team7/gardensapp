package com.example.jhamel.gardenapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

//**Question class
//Each time the user decides to answer a question, they will be redirected to this screen. The layout
//will always be the same, but the text on the buttons and the question will be different.**
public class Question extends AppCompatActivity {

    //**variables to hold the different buttons and text views that we have on the page.**
    Button btn1;
    Button btn2;
    Button btn3;
    Button btn4;
    TextView questionText;

    //**This code is called whenever the activity is created.**
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //**set the layout**
        setContentView(R.layout.activity_question);


        //**have our reference variables point to buttons in the layout file.**
        questionText = (TextView) findViewById(R.id.txtQuestion);
        btn1 = (Button) findViewById(R.id.btnAns1);
        btn2 = (Button) findViewById(R.id.btnAns2);
        btn3 = (Button) findViewById(R.id.btnAns3);
        btn4 = (Button) findViewById(R.id.btnAns4);

        //**set the text for the question and the buttons.**
        questionText.setText(QuestionData.getQuestionText());
        btn1.setText(QuestionData.getButtonText(1));
        btn2.setText(QuestionData.getButtonText(2));
        btn3.setText(QuestionData.getButtonText(3));
        btn4.setText(QuestionData.getButtonText(4));


        //**set up listeners for all buttons.
        //each time a button is clicked, it checks to see if that button was the correct answer. If it is, it
        //will store that information in the SaveData class so that the calling activity can reference it when this activity finishes.**
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (QuestionData.checkAnswer(1)) {
                    SaveData.setButtonDataTrue(SaveData.currentGarden, SaveData.currentScreen, SaveData.currentButton);
                }
                finish();
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(QuestionData.checkAnswer(2)){
                    SaveData.setButtonDataTrue(SaveData.currentGarden, SaveData.currentScreen, SaveData.currentButton);
                }
                finish();
            }
        });


        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(QuestionData.checkAnswer(3)){
                    SaveData.setButtonDataTrue(SaveData.currentGarden, SaveData.currentScreen, SaveData.currentButton);
                }
                finish();
            }
        });




        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(QuestionData.checkAnswer(4)){
                    SaveData.setButtonDataTrue(SaveData.currentGarden, SaveData.currentScreen, SaveData.currentButton);
                }
                finish();
            }
        });


    }
}
