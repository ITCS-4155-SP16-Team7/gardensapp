package com.example.jhamel.gardenapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Harwood_5 extends AppCompatActivity {

    Button btnLeft, btnQues1_5_1, btnQues1_5_2, btnQues1_5_3, btnQues1_5_4, btn_map;

    final int garden = 1;
    final int screen = 5;
    final int button1_5_1 = 1;
    final int button1_5_2 = 2;
    final int button1_5_3 = 3;
    final int button1_5_4 = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_harwood_5);

        SaveData.currentGarden = garden;
        SaveData.currentScreen = screen;

        btnLeft = (Button) findViewById(R.id.btnLeft5);
        btnQues1_5_1 = (Button) findViewById(R.id.btnQ1_5_1);
        btnQues1_5_2 = (Button) findViewById(R.id.btnQ1_5_2);
        btnQues1_5_3 = (Button) findViewById(R.id.btnQ1_5_3);
        btnQues1_5_4 = (Button) findViewById(R.id.btnQ1_5_4);
        btn_map = (Button) findViewById(R.id.btn_goToMap_1_5);


        btn_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.setGoToMap(true);
                finish();
            }
        });

        // paperbush
        btnQues1_5_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.currentButton = button1_5_1;
                QuestionData.setCorrectAnswer(3);
                QuestionData.setQuestionText("Oriental ______  ______ is a beautiful, winter-flowering " +
                        "scented shrub, which has bark valued for making high-quality paper in Japan.");
                QuestionData.setButtonText(1, "pink azalea");
                QuestionData.setButtonText(2, "bark flower");
                QuestionData.setButtonText(3, "paper bush");
                QuestionData.setButtonText(4, "flat tree");
                Intent intent = new Intent(v.getContext(), Question.class);
                startActivity(intent);
            }
        });


        // nandina
        btnQues1_5_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.currentButton = button1_5_2;
                QuestionData.setCorrectAnswer(1);
                QuestionData.setQuestionText("Despite the common name, it is not a bamboo " +
                        "but an erect evergreen shrub up to 2 m (7 ft)");
                QuestionData.setButtonText(1, "Nandina");
                QuestionData.setButtonText(2, "Cornus florida");
                QuestionData.setButtonText(3, "Asclepias syriaca");
                QuestionData.setButtonText(4, "Plantago major");
                Intent intent = new Intent(v.getContext(), Question.class);
                startActivity(intent);
            }
        });

        // first lady dogwood
        btnQues1_5_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.currentButton = button1_5_3;
                QuestionData.setCorrectAnswer(3);
                QuestionData.setQuestionText("The state tree of Virginia, commonly know as 'First Lady':");
                QuestionData.setButtonText(1, "Rudbeckia fulgida");
                QuestionData.setButtonText(2, "Barbarea verna");
                QuestionData.setButtonText(3, "Cornus florida");
                QuestionData.setButtonText(4, "Cornus kousa");
                Intent intent = new Intent(v.getContext(), Question.class);
                startActivity(intent);
            }
        });

        // evergreen witchhazel
        btnQues1_5_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.currentButton = button1_5_4;
                QuestionData.setCorrectAnswer(3);
                QuestionData.setQuestionText("An evergreen witchazel with deep green narrow leaves. " +
                        "The 3'-4' tall plant makes an arching shrub that will bloom with red puffs " +
                        "of flowers in early spring. ");
                QuestionData.setButtonText(1, "Solanum nigrum");
                QuestionData.setButtonText(2, "Sonchus arvensis");
                QuestionData.setButtonText(3, "Distylium myricoides");
                QuestionData.setButtonText(4, "Moringa oleifera");
                Intent intent = new Intent(v.getContext(), Question.class);
                startActivity(intent);
            }
        });

        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(SaveData.getButtonData(garden, screen, button1_5_1)){
            btnQues1_5_1.setVisibility(View.INVISIBLE);
        }
        else{
            btnQues1_5_1.setVisibility(View.VISIBLE);
        }

        if(SaveData.getButtonData(garden, screen, button1_5_2)){
            btnQues1_5_2.setVisibility(View.INVISIBLE);
        }
        else{
            btnQues1_5_2.setVisibility(View.VISIBLE);
        }

        if(SaveData.getButtonData(garden, screen, button1_5_3)){
            btnQues1_5_3.setVisibility(View.INVISIBLE);
        }
        else{
            btnQues1_5_3.setVisibility(View.VISIBLE);
        }

        if(SaveData.getButtonData(garden, screen, button1_5_4)){
            btnQues1_5_4.setVisibility(View.INVISIBLE);
        }
        else{
            btnQues1_5_4.setVisibility(View.VISIBLE);
        }

        if(SaveData.getGoToMap() == true){
            finish();
        }
    }
}
