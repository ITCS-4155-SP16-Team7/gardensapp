package com.example.jhamel.gardenapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Harwood_3 extends AppCompatActivity {

    Button btnLeft, btnRight, btnQues1_3_1, btnQues1_3_2, btnQues1_3_3, btn_map;

    final int garden = 1;
    final int screen = 3;
    final int button1_3_1 = 1;
    final int button1_3_2 = 2;
    final int button1_3_3 = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_harwood_3);

        SaveData.currentGarden = garden;
        SaveData.currentScreen = screen;

        btnLeft = (Button) findViewById(R.id.btnLeft3);
        btnRight = (Button) findViewById(R.id.btnRight3);
        btnQues1_3_1 = (Button) findViewById(R.id.btnQ1_3_1);
        btnQues1_3_2 = (Button) findViewById(R.id.btnQ1_3_2);
        btnQues1_3_3 = (Button) findViewById(R.id.btnQ1_3_3);
        btn_map = (Button) findViewById(R.id.btn_goToMap_1_3);

        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), Harwood_4.class);
                startActivity(intent);
            }
        });

        btn_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.setGoToMap(true);
                finish();
            }
        });

        // Beni maiko
        btnQues1_3_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.currentButton = button1_3_1;
                QuestionData.setCorrectAnswer(3);
                QuestionData.setQuestionText("Emerging foliage in spring is brilliant fire engine red" +
                        " turning pinkish before turning totally green by late May in Georgia.");
                QuestionData.setButtonText(1, "Betula lenta 'Spice birch'");
                QuestionData.setButtonText(2, "Rhus glabra 'Laciniata'");
                QuestionData.setButtonText(3, "Acer palmatum 'Beni maiko'");
                QuestionData.setButtonText(4, "White cornel – Cornus florida");
                Intent intent = new Intent(v.getContext(), Question.class);
                startActivity(intent);
            }
        });

        // azalea
        btnQues1_3_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.currentButton = button1_3_2;
                QuestionData.setCorrectAnswer(4);
                QuestionData.setQuestionText("This beautiful pink plant is very common on the east coast" +
                        " and in small gardens:");
                QuestionData.setButtonText(1, "Polygonatum odoratum 'Variegatum");
                QuestionData.setButtonText(2, "Rudbeckia fulgida var. sullivantii 'Goldsturm'");
                QuestionData.setButtonText(3, "Veronica 'Sunny Border Blue'");
                QuestionData.setButtonText(4, "Azalea 'Autumn Cheer'");
                Intent intent = new Intent(v.getContext(), Question.class);
                startActivity(intent);
            }
        });

        // Not sure here which plant..
        btnQues1_3_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.currentButton = button1_3_3;
                QuestionData.setCorrectAnswer(4);
                QuestionData.setQuestionText("This beautiful pink plant is very common on the east coast" +
                        " and in small gardens: (not sure which plant this refers to..)");
                QuestionData.setButtonText(1, "Polygonatum odoratum 'Variegatum");
                QuestionData.setButtonText(2, "Rudbeckia fulgida var. sullivantii 'Goldsturm'");
                QuestionData.setButtonText(3, "Veronica 'Sunny Border Blue'");
                QuestionData.setButtonText(4, "Azalea 'Autumn Cheer'");
                Intent intent = new Intent(v.getContext(), Question.class);
                startActivity(intent);
            }
        });

        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(SaveData.getButtonData(garden, screen, button1_3_1)){
            btnQues1_3_1.setVisibility(View.INVISIBLE);
        }
        else{
            btnQues1_3_1.setVisibility(View.VISIBLE);
        }

        if(SaveData.getButtonData(garden, screen, button1_3_2)){
            btnQues1_3_2.setVisibility(View.INVISIBLE);
        }
        else{
            btnQues1_3_2.setVisibility(View.VISIBLE);
        }

        if(SaveData.getButtonData(garden, screen, button1_3_3)){
            btnQues1_3_3.setVisibility(View.INVISIBLE);
        }
        else{
            btnQues1_3_3.setVisibility(View.VISIBLE);
        }

        if(SaveData.getGoToMap() == true){
            finish();
        }
    }
}
