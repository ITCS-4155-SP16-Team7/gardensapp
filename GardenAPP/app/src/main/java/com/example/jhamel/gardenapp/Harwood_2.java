package com.example.jhamel.gardenapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

//This is the second screen of the first garden.
public class Harwood_2 extends AppCompatActivity {

    //creates a reference variables for the buttons
    Button btnRight, btnLeft, btnQues1_2_1, btnQues1_2_2, btnQues1_2_3, btn_map;

    final int garden = 1;
    final int screen = 2;
    final int button1_2_1 = 1;
    final int button1_2_2 = 2;
    final int button1_2_3 = 3;

    //This is the overrides method for onCreate.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set the layout
        setContentView(R.layout.activity_harwood_2);

        SaveData.currentGarden = garden;
        SaveData.currentScreen = screen;

        //have our reference variables point to the buttons in the layout file.
        btnLeft = (Button) findViewById(R.id.btnLeft2);
        btnRight = (Button) findViewById(R.id.btnRight2);
        btnQues1_2_1 = (Button) findViewById(R.id.btnQ1_2_1);
        btnQues1_2_2 = (Button) findViewById(R.id.btnQ1_2_2);
        btnQues1_2_3 = (Button) findViewById(R.id.btnQ1_2_3);
        btn_map = (Button) findViewById(R.id.btn_goToMap_1_2);

        //Set a listener for the right arrow button that will bring the user to the Harwood_3 activity when clicked.
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), Harwood_3.class);
                startActivity(intent);
            }
        });

        btn_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.setGoToMap(true);
                finish();
            }
        });

        // Aesculus hippocastanum
        btnQues1_2_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.currentButton = button1_2_1;
                QuestionData.setCorrectAnswer(2);
                QuestionData.setQuestionText("The common name for this plant is: Horse chestnut");
                QuestionData.setButtonText(1, "Polygala chamaebuxus");
                QuestionData.setButtonText(2, "Aesculus hippocastanum");
                QuestionData.setButtonText(3, "Salvia regla");
                QuestionData.setButtonText(4, "Iochroma coccinea");
                Intent intent = new Intent(v.getContext(), Question.class);
                startActivity(intent);
            }
        });

        // Big Blue
        btnQues1_2_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.currentButton = button1_2_2;
                QuestionData.setCorrectAnswer(4);
                QuestionData.setQuestionText("You can find blue flower spikes from this plant:");
                QuestionData.setButtonText(1, "Salvia 'Mainacht'");
                QuestionData.setButtonText(2, "Heuchera micrantha 'Palace Purple'");
                QuestionData.setButtonText(3, "Astilbe 'Sprite'");
                QuestionData.setButtonText(4, "Liriope muscari 'Big Blue'");
                Intent intent = new Intent(v.getContext(), Question.class);
                startActivity(intent);
            }
        });

        // Oleander
        btnQues1_2_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.currentButton = button1_2_3;
                QuestionData.setCorrectAnswer(1);
                QuestionData.setQuestionText("This highly toxic pink shrub was grown as early as roman times and seen in pompeii:");
                QuestionData.setButtonText(1, "Nerium oleander (oleander)");
                QuestionData.setButtonText(2, "Liriope muscari 'Big Blue'");
                QuestionData.setButtonText(3, "Salvia regla");
                QuestionData.setButtonText(4, "Geranium ‘Rozanne’");
                Intent intent = new Intent(v.getContext(), Question.class);
                startActivity(intent);
            }
        });

        //Set a listener for the left arrow button that will end the activity. When the activity is ended, it will be taken out of
        //memory and the app will give control to the next activity on the stack. In this case, the activity that gains control
        // will always be Hargrove_1.
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(SaveData.getButtonData(garden, screen, button1_2_1)){
            btnQues1_2_1.setVisibility(View.INVISIBLE);
        }
        else{
            btnQues1_2_1.setVisibility(View.VISIBLE);
        }

        if(SaveData.getButtonData(garden, screen, button1_2_2)){
            btnQues1_2_2.setVisibility(View.INVISIBLE);
        }
        else{
            btnQues1_2_2.setVisibility(View.VISIBLE);
        }

        if(SaveData.getButtonData(garden, screen, button1_2_3)){
            btnQues1_2_3.setVisibility(View.INVISIBLE);
        }
        else{
            btnQues1_2_3.setVisibility(View.VISIBLE);
        }

        if(SaveData.getGoToMap() == true){
            finish();
        }
    }
}
