package com.example.jhamel.gardenapp;

//**This class holds information about our Question activity.
public final class QuestionData {

    //**this holds the index to the correct answer**
    private static boolean correctAnswer[] = new boolean[4];

    //**this holds the text for each button**
    private static String buttonText [] = new String[4];

    //**this holds the text for the question**
    private static String questionText;


    private QuestionData(){
    }

    //**set the text for the question**
    public static void setQuestionText(String myText){
        questionText = myText;
    }

    //**get the text for the question**
    public static String getQuestionText(){
        return questionText;
    }

    //**set up the correct answer by passing in the c as the index for the correct answer.
    //c must be between 1 and 4. If c is not between 1 and 4, all of the answers will be marked as incorrect.**
    public static void setCorrectAnswer(int c){
        if(c <= 4 && c > 0){
            for(int i = 0; i < correctAnswer.length; i++){
                if(i == c-1){
                    correctAnswer[i] = true;
                }else{
                    correctAnswer[i] = false;
                }
            }
        }
    }

    //**This function takes an index value and checks to see if that index refers to the correct answer. If it
    //does, it will return true. Otherwise it will return false.**
    public static boolean checkAnswer(int index){
        if(index <= 4 && index > 0){
            return correctAnswer[index-1];
        }else{
            return false;
        }
    }

    //**This function takes in an index and a string. It stores the string into the buttonText array for later use.**
    public static void setButtonText(int index, String myTxt){
        if(index <= 4 && index > 0){
            buttonText[index-1] = myTxt;
        }
    }

    //**Use the index to get the text for stored in the buttonText array.**
    public static String getButtonText(int index){
        if(index <= 4 && index > 0){
            return buttonText[index-1];
        }else{
            return "No String";
        }
    }



}
