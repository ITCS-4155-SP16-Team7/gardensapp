package com.example.jhamel.gardenapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

public class Settings extends AppCompatActivity {

    Button btn_resetQ, btn_resetGP, btn_apply, btn_finish;
    CheckBox check_QTrue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        btn_resetQ = (Button) findViewById(R.id.btn_setQuestionsFalse);
        btn_resetGP = (Button) findViewById(R.id.btn_resetGnomePoints);
        btn_apply = (Button) findViewById(R.id.btn_applySettings);
        btn_finish = (Button) findViewById(R.id.btn_goBack);
        check_QTrue = (CheckBox) findViewById(R.id.check_setQuestionsTrue);

        if(SaveData.getHideButtonsMarked()){
            check_QTrue.setChecked(true);
        }


        btn_resetQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.resetButtonData();
                Toast.makeText(Settings.this, "All question progress has been reset", Toast.LENGTH_LONG).show();
            }
        });

        /*
        btn_resetGP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.resetButtonData();
                Toast.makeText(Settings.this, "All Gnome Points have been reset", Toast.LENGTH_LONG).show();
            }
        });
        */

        btn_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(check_QTrue.isChecked()){
                    SaveData.resetButtonDataTrue();
                    SaveData.setHideButtonsMarked(true);
                }
                else{
                    SaveData.setHideButtonsMarked(false);
                }
            }
        });

        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
