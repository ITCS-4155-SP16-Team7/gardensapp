package com.example.jhamel.gardenapp;

//DO NOT ALTER THIS CLASS UNDER ANY CIRCUMSTANCES!!! ALTERING THIS CLASS CAN CAUSE UNEXPECTED RESULTS THROUGHOUT THE ENTIRE APP!!!
//You may however add functionality to this class if needed, but if you add functionality to the class be sure that all fields and functions are static.
//This class contains all the saved data for the app. It is used to save any progress that the user makes while
//using the app. This class does not need to be instantiated in order to use because all the fields and functions are static.
public final class SaveData {

    //these 3 values are used to determine the size of buttonData. The fields refer to the number of gardens that we currently have
    //implemented, the maximum number of screens for each garden, and the maximum number of buttons that can occur on a screen.
    //THESE 3 FIELDS MAY BE EXPANDED IF MORE ROOM IS NEEDED IN THE ARRAY; however I have allocated extra memory to these fields to ensure
    //that we do not run out of space.
    private final static int NUM_GARDENS = 10;
    private final static int NUM_SCREENS = 7;
    private  final static int NUM_BUTTONS = 5;

    public static int currentGarden;
    public static int currentScreen;
    public static int currentButton;

    //if true go back to map. if false, do nothing.
    private static boolean goToMap = false;

    private static boolean hideButtonsMarked = false;

    //this field is used to keep track of which questions the user has entered correctly.
    //It should start with all values being "false", meaning that the user either answered the question incorrectly or did not yet answer the question.
    //The indexes of correct answers should be changed to "true".
    //Be sure to use the class functions to appropriately change the values in this array whenever the user makes progress.
    private static boolean[][][] buttonData = new boolean[NUM_GARDENS][NUM_SCREENS][NUM_BUTTONS];

    //Constructor for class.
    //this will never be used since we never need to instantiate the class.
    private SaveData(){
    }


    //set hideButtonsMarked
    public static void setHideButtonsMarked(boolean b){
        hideButtonsMarked = b;
    }

    //get hideButtonsMarked
    public static boolean getHideButtonsMarked(){
        return hideButtonsMarked;
    }

    //set goToMap
    public static void setGoToMap(boolean b){
        goToMap = b;
    }

    //check state of goToMap
    public static boolean getGoToMap(){
        return goToMap;
    }

    //this function will reset all indexes of buttonData to false. only call this if you want to reset ALL
    //of the user's progress. You can also call this if the user is starting up the app for the first time to
    //ensure the user does not start out with any fields being true.
    public static boolean[][][] resetButtonData(){
        for(int i = 0; i<buttonData.length; i++){
            for(int j = 0; j<buttonData[0].length; j++){
                for(int k = 0; k<buttonData[0][0].length; k++) {
                    buttonData[i][j][k] = false;
                }
            }
        }
        return SaveData.buttonData;
    }

    //reset all button data to true. This will make the buttons invisible.
    public static boolean[][][] resetButtonDataTrue(){
        for(int i = 0; i<buttonData.length; i++){
            for(int j = 0; j<buttonData[0].length; j++){
                for(int k = 0; k<buttonData[0][0].length; k++) {
                    buttonData[i][j][k] = true;
                }
            }
        }
        return SaveData.buttonData;
    }

    //This function is used to change a specified value of buttonData to true; its parameters (i, j, k) are used to refer to the index
    //that needs to be changed. note that this function will not do anything if it is passed parameters that would be out of bounds.
    public static void setButtonDataTrue(int i, int j, int k){
        if(i < buttonData.length && j < buttonData[0].length && k < buttonData[0][0].length){
            if(i>0 && j>0 && k>0) {
                buttonData[i - 1][j - 1][k - 1] = true;
            }
        }
    }

    //This function works just like setButtonDataTrue except it sets the value to false.
    public static void setButtonDataFalse(int i, int j, int k){
        if(i < buttonData.length && j < buttonData[0].length && k < buttonData[0][0].length){
            if(i>0 && j>0 && k>0) {
                buttonData[i - 1][j - 1][k - 1] = false;
            }
        }
    }

    //This function will fetch the boolean value held in buttonData. It uses i, j, and k to determine the index. If the index
    //is out of bounds, this function will return false.
    public static boolean getButtonData(int i, int j, int k){
        if(i < buttonData.length && j < buttonData[0].length && k < buttonData[0][0].length){
            if(i>0 && j>0 && k>0) {
                return buttonData[i - 1][j - 1][k - 1];
            }
        }
        return false;
    }
}
